var csv = require("csvtojson");
const {
  getMatchesPlayedPerSeason,
  getMatchesPlayedPerSeasonPerTeam,
  batsmanRunPerseasonsA,
  batsmanRunPerseasonsB,
  extraRunPerTeam,
  economicalBowler
  } = require("./ipl.js");
const fs = require("fs");
console.time();
csv()
  .fromFile("../data/matches.csv")
  .then((matchjson)=> {
    csv()
      .fromFile("../data/deliveries.csv")
      .then((deliveriesJson) =>{
        console.time();
        batsmanRunPerseasonsA(matchjson, deliveriesJson, "KL Rahul");
        console.timeEnd();
        console.time();
        batsmanRunPerseasonsB(matchjson, deliveriesJson, "KL Rahul");
        console.timeEnd();
        //    sendData(batsmanRunPerseason,matchjson,deliveriesJson,"batsmanRunPerseason.json","KL Rahul");

        //  sendData(getMatchesPlayedPerSeason,matchjson,deliveriesJson,"getMatchesPlayedPerSeason.json",null);
          sendData(getMatchesPlayedPerSeasonPerTeam,matchjson,deliveriesJson,"getMatchesPlayedPerSeasonPerTeam.json",null);
        //  sendData(extraRunPerTeam,matchjson,deliveriesJson,"extraRunPerTeam.json",2016);
        //  sendData(economicalBowler,matchjson,deliveriesJson,"economicalBowler.json",2015);
        
      });
  });

function sendData(function_name,matchJson,deliveriesJson,filenamee,year){


  if((function_name == getMatchesPlayedPerSeason) || (function_name == getMatchesPlayedPerSeasonPerTeam)){
  
  var jsonContent = JSON.stringify( function_name(matchJson),null,4);
  console.log(jsonContent);
}else{
  var jsonContent = JSON.stringify( function_name(matchJson,deliveriesJson,year),null,4);
  //console.log("calling 3 and 4");
  console.log(jsonContent);
}
  let output="../output/"+filenamee;

fs.writeFile(output, jsonContent, 'utf8', function (err) {
    if (err) {
        console.log("An error occured while writing JSON Object to File.");
        return console.log(err);
    }

    console.log("JSON file has been saved.");
});
}
