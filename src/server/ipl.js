function getMatchesPlayedPerSeason(matches) {
  const matchesPerSeason = matches.reduce((matchesPerSeason, match) => {
    if (matchesPerSeason.hasOwnProperty(match.season)) {
      matchesPerSeason[match.season] = ++matchesPerSeason[match.season];
    } else {
      matchesPerSeason[match.season] = 1;
    }
    return matchesPerSeason;
  }, {});
  //let count=0;

  //console.log(matches)

  // matches.forEach(match => {
  //   // if (match.season in matchesPerSeason) {
  //   if (matchesPerSeason.hasOwnProperty(match.season)) {
  //     //matchesPerSeason[match.season] = (matchesPerSeason[match.season]+1) || 1 ;
  //     matchesPerSeason[match.season] = ++matchesPerSeason[match.season];
  //    // console.log(matchesPerSeason[match.season]);
  //   } else {
  //     matchesPerSeason[match.season] = 1;
  //     // console.log("in else " + matchesPerSeason[match.season]);
  //   }

  // });
  //console.log(year+1);

  //console.log(matchesPerSeason);
  return matchesPerSeason;
}

function getMatchesPlayedPerSeasonPerTeam(matches) {
  const matchPerYearPerTeam = matches.reduce((matchPerYearPerTeam, match) => {
    if (matchPerYearPerTeam[match.season] == undefined) {
      matchPerYearPerTeam[match.season] = {};
    }
    if (matchPerYearPerTeam[match.season][match.winner] === undefined) {
      matchPerYearPerTeam[match.season][match.winner] = 1;
    } else {
      matchPerYearPerTeam[match.season][match.winner] += 1;
    }
    return matchPerYearPerTeam;
  }, {});

  // matches.forEach(element => {
  //  // console.log();

  //   if (a[b.season] == undefined) {
  //     a[b.season] = {};
  //     //a[b.season][b.winner]=1;
  //   }
  //   // a[b.season][b.winner]=(a[b.season][b.winner] || 0) + 1
  //   if (a[b.season][b.winner] === undefined) {
  //     a[b.season][b.winner] = 1;
  //   } else {
  //     a[b.season][b.winner] += 1;
  //   }
  // });
  //console.log(matchPerYearPerTeam);
  return matchPerYearPerTeam;
}
function extraRunPerTeam(matches, deliveries, year) {
  // var  arr = [];
  // let obj={}
  let matchIdArray = matches
    .filter(match => match.season == year)
    .map(match => match.id);
  // for (let i = 0; i < matches.length; i++) {
  //   if (matches[i].season === "2016") {
  //     arr.push(matches[i].id);
  //   }
  // }
  const teamsrun = deliveries.reduce((teamsrun, deliveries) => {
    if (matchIdArray.includes(deliveries["match_id"])) {
      if (teamsrun[deliveries["bowling_team"]] == undefined) {
        teamsrun[deliveries["bowling_team"]] = 0;
      }
      teamsrun[deliveries["bowling_team"]] += parseInt(deliveries.extra_runs);
    }
    return teamsrun;
  }, {});
  // for(let i=0;i<arr.length;i++){
  //   for(let j=0;j<deliveries.length;j++){
  //     if(deliveries[j].match_id == arr[i]  ){
  //       if(obj[deliveries[j].batting_team] == undefined){
  //         obj[deliveries[j].batting_team]=0;
  //       }
  //       obj[deliveries[j].batting_team] = parseInt(obj[deliveries[j].batting_team])+parseInt(deliveries[j].extra_runs)

  //     }

  //   }

  // }

  //console.log(arr);

  console.log(teamsrun);
  // console.log(matches)
  return teamsrun;
}
function economicalBowler(matches, deliveries, year) {
  // let matchIdArray=[];
  //let obj={};
  // for (let i = 0; i < matches.length; i++) {
  //   if (matches[i].season === "2015") {
  //     matchIdArray.push(matches[i].id);
  //   }
  // }
  let matchIdArray = matches
    .filter(match => match.season == year)
    .map(match => match.id);

  //console.log(matchIdArray)

  // console.log(matchIdArray);
  // const res=deliveries.reduce((a,b)=>{

  // });
  const ballersData = deliveries.reduce((ballersData, delivery) => {
    if (matchIdArray.includes(delivery["match_id"])) {
      if (ballersData[delivery.bowler] == undefined) {
        if (delivery["noball_runs"] == 0 && delivery["wide_runs"] == 0) {
          ballersData[delivery.bowler] = [
            parseInt(delivery["total_runs"]) -
              parseInt(delivery["legbye_runs"]) -
              parseInt(delivery["bye_runs"]),
            1
          ];
        } else {
          ballersData[delivery.bowler] = [
            parseInt(delivery["total_runs"]) -
              parseInt(delivery["legbye_runs"]) -
              parseInt(delivery["bye_runs"]),
            0
          ];
        }
      } else {
        if (delivery["noball_runs"] == 0 && delivery["wide_runs"] == 0) {
          ballersData[delivery.bowler][1] = ++ballersData[delivery.bowler][1];
          ballersData[delivery.bowler][0] =
            parseInt(ballersData[delivery.bowler][0]) +
            (parseInt(delivery["total_runs"]) -
              parseInt(delivery["legbye_runs"]) -
              parseInt(delivery["bye_runs"]));
        } else {
          ballersData[delivery.bowler][0] =
            parseInt(ballersData[delivery.bowler][0]) +
            (parseInt(delivery["total_runs"]) -
              parseInt(delivery["legbye_runs"]) -
              parseInt(delivery["bye_runs"]));
          ballersData[delivery.bowler][1] = ballersData[delivery.bowler][1];

          // a[e.bowler] =[parseInt(e["total_runs"])-parseInt(e["legbye_runs"])-parseInt(e["bye_runs"]),0]
        }
        //   a[e.bowler] [1]=parseInt(a[e.bowler] [1]);
        // a[e.bowler] [0]=parseInt(a[e.bowler] [0])+parseInt(e["total_runs"])-parseInt(e["legbye_runs"])-parseInt(e["bye_runs"]);
      }
    }
    return ballersData;
  }, {});
  // const res=deliveries.map(e =>{
  //   if(matchIdArray.includes(e["match_id"])){

  //     if(a[e["bowler"]] == undefined){

  //       a[e["bowler"]]=[parseInt(e["total_runs"]),1]
  //       //console.log(a)
  //     }
  //     a[e["bowler"]][1]=parseInt(a[e["bowler"]][1])+1;
  //     a[e["bowler"]][0]=parseInt(a[e["bowler"]][0])+parseInt(e["total_runs"]);
  //     return "dees";
  //   }

  // });
  //console.log(ballersData);

  // for(let i=0;i< matchIdArray.length;i++ ){
  //   for(let j=0;j<deliveriesjson.length;j++){
  //     if(matchIdArray[i] == deliveriesjson[j]["match_id"] ){
  //       if( obj[deliveriesjson[j]["bowler"]] === undefined){
  //         obj[deliveriesjson[j]["bowler"]]=[parseInt(deliveriesjson[j]["total_runs"]),1]

  //       }
  //      // console.log(obj)
  //       obj[deliveriesjson[j]["bowler"]][1] =parseInt(obj[deliveriesjson[j]["bowler"]][1]) + 1 ;
  //       obj[deliveriesjson[j]["bowler"]][0] =parseInt(obj[deliveriesjson[j]["bowler"]][0]) +parseInt(deliveriesjson[j]["total_runs"]);

  //     }
  //   }
  // }
  for (let ballerName in ballersData) {
    ballersData[ballerName] =
      ballersData[ballerName][0] / (ballersData[ballerName][1] / 6);
  }
  let top10EconomicalBowler = Object.entries(ballersData)
    .sort((a, b) => a[1] - b[1])
    .slice(0, 10);
  //dont uncomment
  //       topEconomical.forEach(element => {
  //       element=element[0]/(element[1]/6);
  // });
  return top10EconomicalBowler;
}
function batsmanRunPerseasonsA(matches, deliveries, playerName) {
  const playerDeliveries = deliveries.filter(
    delivery => delivery.batsman == playerName
  );

  const playerIDs = matches.reduce((playerIDs, match) => {
    playerIDs[match.id] = match.season;
    return playerIDs;
  }, {});
  //console.log(playerIDs);
  const runsPerYear = playerDeliveries.reduce((runsPerYear, playerDelivery) => {
    let currentYear;
    if (playerIDs.hasOwnProperty(playerDelivery.match_id)) {
      currentYear = playerIDs[playerDelivery.match_id];
    }
    if (runsPerYear[currentYear] == undefined) {
      runsPerYear[currentYear] = parseInt(playerDelivery.batsman_runs);
    } else {
      runsPerYear[currentYear] += parseInt(playerDelivery.batsman_runs);
    }

    return runsPerYear;
  }, {});
  console.log(runsPerYear);
}
function batsmanRunPerseasonsB(matches, deliveries, playerName) {
  let matchIdsPerYear = matches.reduce((matchIDObj, match) => {
    if (matchIDObj[match.season] == undefined) {
      matchIDObj[match.season] = {};
      matchIDObj[match.season][match.id] = 1;
    } else {
      matchIDObj[match.season][match.id] = 1;
    }
    return matchIDObj;
  }, {});

  const runsPerYear = deliveries.reduce((runsPerYear, delivery) => {
    let currentYear;
    if (delivery.batsman == playerName) {
      //console.log(delivery.batsman)
      for (let season in matchIdsPerYear) {
        //console.log(matchIdsPerYear[a]);
        if (matchIdsPerYear[season].hasOwnProperty(delivery.match_id)) {
          currentYear = season;
          //console.log(year)
          break;
        }
      }
      //console.log("year------"+year)
      if (runsPerYear[currentYear] == undefined) {
        runsPerYear[currentYear] = parseInt(delivery.batsman_runs);
      } else {
        runsPerYear[currentYear] += parseInt(delivery.batsman_runs);
      }
      return runsPerYear;
    }
    return runsPerYear;
  }, {});
  console.log(runsPerYear);
  //console.log(match_id_per_year)
  return runsPerYear;
}

module.exports = {
  batsmanRunPerseasonsA,
  batsmanRunPerseasonsB,
  getMatchesPlayedPerSeason,
  getMatchesPlayedPerSeasonPerTeam,
  economicalBowler,
  extraRunPerTeam
};
