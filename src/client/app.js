

fetch('http://localhost:9000/getMatchesPlayedPerSeason')
 .then(res=>{
    return res.json();

 }).then(data=>{
     //console.log(data)
    Highcharts.chart('getMatchesPlayedPerSeason', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Match per season'
        },
        // subtitle: {
        //     text: 'Source: WorldClimate.com'
        // },
        xAxis: {
            categories: Object.keys(data)
            // crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Matches (Numbers)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'india',
            data: Object.values(data)
   
        }]
    });
 }).catch(err => console.log(err))

 fetch('http://localhost:9000/getMatchesPlayedPerSeasonPerTeam')
 .then(res=>{
    return res.json();

 }).then(data=>{
    //  console.log(data)
    let res=Object.values(data).reduce( (res,MatchesPerYearObj)=>{
        for(let teamName in MatchesPerYearObj){
            //console.log(MatchesPerYearObj[teamName])
         if(res[teamName] == undefined){
             res[teamName]=[];
            // res[teamName].push(parseInt(MatchesPerYearObj[teamName]))
         }
 }
        return res;
    },{})
    let matchesPerTeamPerScore=Object.values(data).reduce((res,MatchesPerYearObj)=>{
        for(let teamName in res){
                       //console.log(MatchesPerYearObj[teamName])
                      
                    if(MatchesPerYearObj.hasOwnProperty(teamName)){
                      //  console.log("a")
                        res[teamName].push(parseInt(MatchesPerYearObj[teamName]))
                    }
                    else{
                        //console.log("a")

                        res[teamName].push(null);
                    }
                   }
                   return res;
    },res)
   console.log(matchesPerTeamPerScore);
     let ress=[]
     let temp;
     for(let a in matchesPerTeamPerScore){
         temp={}
         temp.name=a;
         temp.data=matchesPerTeamPerScore[a]
        // console.log(temp)
        ress.push(temp);
     }
    console.log(ress)
    Highcharts.chart('getMatchesPlayedPerSeasonPerTeam', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Match per season per Team'
        },
        // subtitle: {
        //     text: 'Source: WorldClimate.com'
        // },
        xAxis: {
            categories: Object.keys(data)
            // crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Matches (Numbers)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: ress
    });
 }).catch(err => console.log(err))


fetch('http://localhost:9000/extraRunPerTeam')
 .then(res=>{
    return res.json();

 }).then(data=>{
    // console.log(data)
    Highcharts.chart('extraRunPerTeam', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Extra Run Per Team'
        },
        // subtitle: {
        //     text: 'Source: WorldClimate.com'
        // },
        xAxis: {
            categories: Object.keys(data)
            // crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Runs'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Run',
            data: Object.values(data)
   
        }]
    });
 }).catch(err => console.log(err))




fetch('http://localhost:9000/economicalBowler')
.then(res=>{
   return res.json();

}).then(data=>{
    let dataObj={};
    data.forEach(element => {
        console.log(typeof element[0])
        dataObj[element[0]]=element[1]
    });
   
   console.log(Object.keys(dataObj))
   Highcharts.chart('economicalBowler', {
       chart: {
           type: 'column'
       },
       title: {
           text: 'Economical Bowler'
       },
      
       xAxis: {
           categories: Object.keys(dataObj),
            crosshair: true
       },
       yAxis: {
           min: 0,
           title: {
               text: 'Economy rate'
           }
       },
       tooltip: {
           headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
           pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
               '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
           footerFormat: '</table>',
           shared: true,
           useHTML: true
       },
       plotOptions: {
           column: {
               pointPadding: 0.2,
               borderWidth: 0
           }
       },
       series: [{
           name: 'Economy',
           data: Object.values(dataObj)
   
       }]
   });
}).catch(err => console.log(err))
















// fetch('../output/batsmanRunPerseason.json')
//  .then(res=>{
//     return res.json();

//  }).then(data=>{
//    //  console.log(data)
//     Highcharts.chart('container', {
//         chart: {
//             type: 'column'
//         },
//         title: {
//             text: 'Monthly Average Rainfall'
//         },
//         subtitle: {
//             text: 'Source: WorldClimate.com'
//         },
//         xAxis: {
//             categories: Object.keys(data)
//             // crosshair: true
//         },
//         yAxis: {
//             min: 0,
//             title: {
//                 text: 'Rainfall (mm)'
//             }
//         },
//         tooltip: {
//             headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//             pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
//                 '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
//             footerFormat: '</table>',
//             shared: true,
//             useHTML: true
//         },
//         plotOptions: {
//             column: {
//                 pointPadding: 0.2,
//                 borderWidth: 0
//             }
//         },
//         series: [{
//             name: 'Tokyo',
//             data: Object.values(data)
   
//         }]
//     });
//  }).catch(err => console.log(err))




